import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Hello World App",
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Hello World App'),
        ),
        body: Center(child: Text('''Hello World
        Nandhinee Kanniappan
        Graduation Date: May 2021
        Success is when your SIGNATURE changes to AUTOGRAPH 
        - APJ Abdul Kalam''',textAlign: TextAlign.center),

        ),
      ),
    );
  }
}